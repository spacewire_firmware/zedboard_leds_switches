//
// File .......... axi_gpio_zed.v
// Author ........ Steve Haywood
// Version ....... 1.0
// Date .......... 29 October 2022
// Standard ...... IEEE 1364-2001
// Description ...
//   AXI4-Lite controlled GPIO block written in basic Verilog. Primary purpose
// is to provide access to the LEDs, Switches & Push Buttons of the Zedboard.
// Design offers an Interrupt that can be thrown when any of the Push Buttons
// are pressed or released. Address map is compatible with the one used in the
// AXI GPIO IP from Xilinx, albeit a cut-down version.
//


`timescale 1 ns / 1 ps


module axi_gpio_zed #
(
  // Parameters (AXI4-Lite)
  localparam c_addr_w = 9,                                // P:Address Bus Width (bits)
  localparam c_data_w = 32,                               // P:Data Bus Width (bits)
  // GPIO Channels
  parameter  c_leds_w     = 8,                            // P:LEDs Width (bits)
  parameter  c_switches_w = 8,                            // P:Switches Width (bits)
  parameter  c_buttons_w  = 5                             // P:Buttons Width (bits)
)
(
  // Clock & Reset
  input  wire                             aclk,           // I:Clock
  input  wire                             aresetn,        // I:Reset
  // GPIO Channels
  output wire [c_leds_w-1 : 0]            leds,           // O:LEDs
  input  wire [c_switches_w-1 : 0]        switches,       // I:Switches
  input  wire [c_buttons_w-1 : 0]         buttons,        // I:Buttons
  // Interrupt

  output wire                             interrupt,      // O:Output
  // AXI4-Lite - Write Address Channel
  input  wire [c_addr_w-1 : 0]            s_axi_awaddr,   // I:Write Address
  input  wire [2 : 0]                     s_axi_awprot,   // I:Write Protection Type
  input  wire                             s_axi_awvalid,  // I:Write Address Valid
  output reg                              s_axi_awready,  // O:Write Address Ready
  // AXI4-Lite - Write Data Channel
  input  wire [c_data_w-1 : 0]            s_axi_wdata,    // I:Write Data
  input  wire [(c_data_w/8)-1 : 0]        s_axi_wstrb,    // I:Write Strobes
  input  wire                             s_axi_wvalid,   // I:Write Valid
  output reg                              s_axi_wready,   // O:Write Ready
  // AXI4-Lite - Write Response Channel
  output reg  [1 : 0]                     s_axi_bresp,    // O:Write Response
  output reg                              s_axi_bvalid,   // O:Write Response Valid
  input  wire                             s_axi_bready,   // I:Write Response Ready
  // AXI4-Lite - Read Address Channel
  input  wire [c_addr_w-1 : 0]            s_axi_araddr,   // I:Read Address
  input  wire [2 : 0]                     s_axi_arprot,   // I:Read Protection Type
  input  wire                             s_axi_arvalid,  // I:Read Address Valid
  output reg                              s_axi_arready,  // O:Read Address Ready
  // AXI4-Lite - Read Data Channel
  output reg  [c_data_w-1 : 0]            s_axi_rdata,    // O:Read Data
  output reg  [1 : 0]                     s_axi_rresp,    // O:Read Response
  output reg                              s_axi_rvalid,   // O:Read Valid
  input  wire                             s_axi_rready    // I:Read Ready
);


  // Constants
  localparam c_bytes = c_data_w / 8;                         // Number of bytes in data bus
  localparam c_registers = 2**(c_addr_w - $clog2(c_bytes));  // Number of registers
  localparam c_channels = 3;                                 // Number of channels

  // Channel positions
  localparam c_chan_leds     = 0;  // LEDs
  localparam c_chan_switches = 1;  // Switches
  localparam c_chan_buttons  = 2;  // Buttons

  // Register positions
  localparam c_reg_leds      = 'h000 / c_bytes;  // LEDs
  localparam c_reg_switches  = 'h008 / c_bytes;  // Switches
  localparam c_reg_buttons   = 'h010 / c_bytes;  // Buttons
  localparam c_reg_isr       = 'h120 / c_bytes;  // Interrupt Status
  localparam c_reg_ier       = 'h128 / c_bytes;  // Interrupt Enable
  localparam c_reg_gie       = 'h11C / c_bytes;  // Global Interrupt Enable

  // Bit positions
  localparam c_gie_gintr_enable = 31; // Global Interrupt Enable

  // Signals
  reg aw_en;
  reg [c_addr_w-$clog2(c_bytes) - 1 : 0] axi_awaddr;
  reg [c_addr_w-$clog2(c_bytes) - 1 : 0] axi_araddr;
  reg [c_data_w-1 : 0] registers [c_registers - 1 : 0];

  wire [c_buttons_w+c_switches_w-1 : 0] inp;   // Debouncer Inputs
  wire [c_buttons_w+c_switches_w-1 : 0] outp;  // Debouncer Outputs

  wire [c_switches_w-1 : 0] switches_int;  // Debounced Switches
  wire [c_buttons_w-1  : 0] buttons_int;   // Debounced Buttons
  reg  [c_buttons_w-1  : 0] buttons_reg;   // Registered Buttons

  reg  [c_chan_buttons : c_chan_buttons] isr;  // Interrupt Status


  // Write Address Ready
  always @(posedge aclk)
    if (!aresetn) begin
      s_axi_awready <= 1'b0;
      aw_en <= 1'b1;
    end else if (s_axi_awvalid && !s_axi_awready && s_axi_wvalid && aw_en) begin
      s_axi_awready <= 1'b1;
      aw_en <= 1'b0;
    end else if (s_axi_bvalid && s_axi_bready) begin
      s_axi_awready <= 1'b0;
      aw_en <= 1'b1;
    end else
      s_axi_awready <= 1'b0;


  // Write Address Latch
  always @(posedge aclk)
    if (!aresetn)
      axi_awaddr <= {c_addr_w{1'b0}};
    else if (s_axi_awvalid && !s_axi_awready && s_axi_wvalid && aw_en)
      axi_awaddr <= s_axi_awaddr[c_addr_w - 1 : $clog2(c_bytes)];


  // Write Data Ready
  always @(posedge aclk)
    if (!aresetn)
      s_axi_wready <= 1'b0;
    else if (s_axi_awvalid && s_axi_wvalid && !s_axi_wready && aw_en)
      s_axi_wready <= 1'b1;
    else
      s_axi_wready <= 1'b0;


  // Write Data
  integer i;
  always @(posedge aclk)
    if (!aresetn)
      for (i = 0; i <= c_registers - 1; i = i + 1)
        registers[i] <= {c_data_w{1'b0}};
    else begin
      // Latch & hold Push Buttons
      for (i = 0; i < c_buttons_w; i = i + 1)
        if (buttons_int[i])
          registers[c_reg_buttons][i] <= 1'b1;
      // Special handling for toggle-on-write
      registers[c_reg_isr] <= {c_data_w{1'b0}};
      if (s_axi_awvalid && s_axi_awready && s_axi_wvalid && s_axi_wready)
        for (i = 0; i <= c_bytes - 1; i = i + 1)
          if (s_axi_wstrb[i])
            registers[axi_awaddr][i*8 +: 8] <= s_axi_wdata[i*8 +: 8]; // Byte-wise write
    end


  // Write Response
  always @(posedge aclk)
    if (!aresetn) begin
      s_axi_bvalid <= 1'b0;
      s_axi_bresp  <= 2'b00;
    end else if (s_axi_awvalid && s_axi_awready && s_axi_wvalid && s_axi_wready && !s_axi_bvalid) begin
      s_axi_bvalid <= 1'b1;
      s_axi_bresp  <= 2'b00;
    end else if (s_axi_bvalid && s_axi_bready)
      s_axi_bvalid <= 1'b0;


  // Read Address Ready
  always @(posedge aclk)
    if (!aresetn)
      s_axi_arready <= 1'b0;
    else if (s_axi_arvalid && !s_axi_arready)
      s_axi_arready <= 1'b1;
    else
      s_axi_arready <= 1'b0;


  // Read Address Latch
  always @(posedge aclk)
    if (!aresetn)
      axi_araddr  <= {c_addr_w{1'b0}};
    else if (s_axi_arvalid && !s_axi_arready)
      axi_araddr <= s_axi_araddr[c_addr_w - 1 : $clog2(c_bytes)];


  // Read Response
  always @(posedge aclk)
    if (!aresetn) begin
      s_axi_rvalid <= 1'b0;
      s_axi_rresp  <= 2'b00;
    end else if (s_axi_arvalid && s_axi_arready && !s_axi_rvalid) begin
      s_axi_rvalid <= 1'b1;
      s_axi_rresp  <= 2'b00;
    end else if (s_axi_rvalid && s_axi_rready)
      s_axi_rvalid <= 1'b0;


  // Read Data
  always @(posedge aclk)
    if (!aresetn)
      s_axi_rdata <= {c_data_w{1'b0}};
    else if (s_axi_arvalid && s_axi_arready && !s_axi_rvalid) begin
      // Default
      s_axi_rdata <= {c_data_w{1'b0}};
      // Select
      case (axi_araddr)
        c_reg_leds     : s_axi_rdata[c_leds_w-1     : 0             ] <= registers[c_reg_leds][c_leds_w-1:0];
        c_reg_switches : s_axi_rdata[c_switches_w-1 : 0             ] <= switches_int;
        c_reg_buttons  : s_axi_rdata[c_buttons_w-1  : 0             ] <= registers[c_reg_buttons][c_buttons_w-1:0];
        c_reg_isr      : s_axi_rdata[c_chan_buttons : c_chan_buttons] <= isr;
        c_reg_ier      : s_axi_rdata[c_chan_buttons : c_chan_buttons] <= registers[c_reg_ier][c_chan_buttons : c_chan_buttons];
        c_reg_gie      : s_axi_rdata[c_gie_gintr_enable             ] <= registers[c_reg_gie][c_gie_gintr_enable];
        default        : s_axi_rdata                                  <= {c_data_w{1'b0}};
      endcase
    end


  // Debouncer Connections
  assign inp = {buttons, switches};
  assign {buttons_int, switches_int} = outp;


  // Debouncer
  genvar j;
  generate for (j = 0; j < c_buttons_w + c_switches_w; j = j + 1)
    debounce #
     (
      // Parameters
      .c_cycles ( 65536   )   // Debounce Cycles
     )
    debounce
     (
      // Clock & Reset
      .aclk     ( aclk    ),  // Clock
      .aresetn  ( aresetn ),  // Reset
      // Input & Output
      .inp      ( inp[j]  ),  // Input (Dirty)
      .outp     ( outp[j] )   // Output (Clean)
     );
  endgenerate


  // Register Buttons for Edge Detection
  always @(posedge aclk)
    if (!aresetn)
      buttons_reg <= {c_buttons_w{1'b0}};
    else
      buttons_reg <= buttons_int;


  // Throw an Interrupt when a button is pressed or released
  always @(posedge aclk)
    if (!aresetn)
      isr[c_chan_buttons] <= 1'b0;
    else if (registers[c_reg_gie][c_gie_gintr_enable] && registers[c_reg_ier][c_chan_buttons]) begin
//    if (|(~buttons_reg & buttons_int)) // Button(s) pressed
      if (|(buttons_reg & ~buttons_int)) // Button(s) released
//    if (buttons_reg != buttons_int) // Button(s) changed state
        isr[c_chan_buttons] <= 1'b1;
      else if (registers[c_reg_isr][c_chan_buttons])
        isr[c_chan_buttons] <= !isr[c_chan_buttons]; // Can be set & cleared when Interrupt is enabled
    end else if (registers[c_reg_isr][c_chan_buttons])
      isr[c_chan_buttons] <= 1'b0; // Can only be cleared when Interrupt is disabled


  // Interrupt output
  assign interrupt = |isr;


  // LEDs output
  assign leds = registers[c_reg_leds][c_leds_w-1:0];


endmodule
