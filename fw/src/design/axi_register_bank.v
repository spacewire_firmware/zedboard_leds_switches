//
// File .......... axi_register_bank.v
// Author ........ Steve Haywood
// Version ....... 1.0
// Date .......... 29 October 2022
// Standard ...... IEEE 1364-2001
// Description ...
//   AXI4-Lite controlled general purpose register bank written in basic
// Verilog. This module is intended to be a building block for something more
// useful.
//
// With c_data_w = 32 the number of registers is as follows :-
//
// c_addr_w - c_registers
// ~~~~~~~~ - ~~~~~~~~~~~
// 2        -   1
// 3        -   2
// 4        -   4
// 5        -   8
// 6        -  16
// 7        -  32
// 8        -  64
// 9        - 128
// 10       - 256
//


`timescale 1 ns / 1 ps


module axi_register_bank #
(
  // Parameters
  parameter  c_addr_w = 4,                                // P:Address Bus Width (bits)
  localparam c_data_w = 32                                // P:Data Bus Width (bits)
)
(
  // Clock & Reset
  input  wire                             aclk,           // I:Clock
  input  wire                             aresetn,        // I:Reset
  // AXI4-Lite - Write Address Channel
  input  wire [c_addr_w-1 : 0]            s_axi_awaddr,   // I:Write Address
  input  wire [2 : 0]                     s_axi_awprot,   // I:Write Protection Type
  input  wire                             s_axi_awvalid,  // I:Write Address Valid
  output reg                              s_axi_awready,  // O:Write Address Ready
  // AXI4-Lite - Write Data Channel
  input  wire [c_data_w-1 : 0]            s_axi_wdata,    // I:Write Data
  input  wire [(c_data_w/8)-1 : 0]        s_axi_wstrb,    // I:Write Strobes
  input  wire                             s_axi_wvalid,   // I:Write Valid
  output reg                              s_axi_wready,   // O:Write Ready
  // AXI4-Lite - Write Response Channel
  output reg  [1 : 0]                     s_axi_bresp,    // O:Write Response
  output reg                              s_axi_bvalid,   // O:Write Response Valid
  input  wire                             s_axi_bready,   // I:Write Response Ready
  // AXI4-Lite - Read Address Channel
  input  wire [c_addr_w-1 : 0]            s_axi_araddr,   // I:Read Address
  input  wire [2 : 0]                     s_axi_arprot,   // I:Read Protection Type
  input  wire                             s_axi_arvalid,  // I:Read Address Valid
  output reg                              s_axi_arready,  // O:Read Address Ready
  // AXI4-Lite - Read Data Channel
  output reg  [c_data_w-1 : 0]            s_axi_rdata,    // O:Read Data
  output reg  [1 : 0]                     s_axi_rresp,    // O:Read Response
  output reg                              s_axi_rvalid,   // O:Read Valid
  input  wire                             s_axi_rready    // I:Read Ready
);


  // Constants
  localparam c_bytes = c_data_w / 8;                         // Number of bytes in data bus
  localparam c_registers = 2**(c_addr_w - $clog2(c_bytes));  // Number of registers

  // Signals
  reg aw_en;
  reg [c_addr_w-$clog2(c_bytes) - 1 : 0] axi_awaddr;
  reg [c_addr_w-$clog2(c_bytes) - 1 : 0] axi_araddr;
  reg [c_data_w-1 : 0] registers [c_registers - 1 : 0];


  // Write Address Ready
  always @(posedge aclk)
    if (!aresetn) begin
      s_axi_awready <= 1'b0;
      aw_en <= 1'b1;
    end else if (s_axi_awvalid && !s_axi_awready && s_axi_wvalid && aw_en) begin
      s_axi_awready <= 1'b1;
      aw_en <= 1'b0;
    end else if (s_axi_bvalid && s_axi_bready) begin
      s_axi_awready <= 1'b0;
      aw_en <= 1'b1;
    end else
      s_axi_awready <= 1'b0;


  // Write Address Latch
  always @(posedge aclk)
    if (!aresetn)
      axi_awaddr <= {c_addr_w{1'b0}};
    else if (s_axi_awvalid && !s_axi_awready && s_axi_wvalid && aw_en)
      axi_awaddr <= s_axi_awaddr[c_addr_w - 1 : $clog2(c_bytes)];


  // Write Data Ready
  always @(posedge aclk)
    if (!aresetn)
      s_axi_wready <= 1'b0;
    else if (s_axi_awvalid && s_axi_wvalid && !s_axi_wready && aw_en)
      s_axi_wready <= 1'b1;
    else
      s_axi_wready <= 1'b0;


  // Write Data
  integer i;
  always @(posedge aclk)
    if (!aresetn)
      for (i = 0; i <= c_registers - 1; i = i + 1)
        registers[i] <= {c_data_w{1'b0}};
    else begin
      if (s_axi_awvalid && s_axi_awready && s_axi_wvalid && s_axi_wready)
        for (i = 0; i <= c_bytes - 1; i = i + 1)
          if (s_axi_wstrb[i])
            registers[axi_awaddr][i*8 +: 8] <= s_axi_wdata[i*8 +: 8]; // Byte-wise write
    end


  // Write Response
  always @(posedge aclk)
    if (!aresetn) begin
      s_axi_bvalid <= 1'b0;
      s_axi_bresp  <= 2'b00;
    end else if (s_axi_awvalid && s_axi_awready && s_axi_wvalid && s_axi_wready && !s_axi_bvalid) begin
      s_axi_bvalid <= 1'b1;
      s_axi_bresp  <= 2'b00;
    end else if (s_axi_bvalid && s_axi_bready)
      s_axi_bvalid <= 1'b0;


  // Read Address Ready
  always @(posedge aclk)
    if (!aresetn)
      s_axi_arready <= 1'b0;
    else if (s_axi_arvalid && !s_axi_arready)
      s_axi_arready <= 1'b1;
    else
      s_axi_arready <= 1'b0;


  // Read Address Latch
  always @(posedge aclk)
    if (!aresetn)
      axi_araddr  <= {c_addr_w{1'b0}};
    else if (s_axi_arvalid && !s_axi_arready)
      axi_araddr <= s_axi_araddr[c_addr_w - 1 : $clog2(c_bytes)];


  // Read Response
  always @(posedge aclk)
    if (!aresetn) begin
      s_axi_rvalid <= 1'b0;
      s_axi_rresp  <= 2'b00;
    end else if (s_axi_arvalid && s_axi_arready && !s_axi_rvalid) begin
      s_axi_rvalid <= 1'b1;
      s_axi_rresp  <= 2'b00;
    end else if (s_axi_rvalid && s_axi_rready)
      s_axi_rvalid <= 1'b0;


  // Read Data
  always @(posedge aclk)
    if (!aresetn)
      s_axi_rdata <= {c_data_w{1'b0}};
    else if (s_axi_arvalid && s_axi_arready && !s_axi_rvalid)
      s_axi_rdata <= registers[axi_araddr];


endmodule 
