//
// File .......... axi_identification.v
// Author ........ Steve Haywood
// Version ....... 1.1
// Date .......... 2 October 2022
// Standard ...... IEEE 1364-2001
// Description ...
//   AXI4-Lite controlled register bank written in basic Verilog. The register
// bank contains the following fixed length identification and timestamp
// strings.
//
// Description ................ 128 Characters - 32 Registers - 0x000 - 0x07F
// Company ....................  64 Characters - 16 Registers - 0x080 - 0x0BF
// Author .....................  64 Characters - 16 Registers - 0x0C0 - 0x0FF
// Version ....................  32 Characters -  8 Registers - 0x100 - 0x11F
// Timestamp ..................  32 Characters -  8 Registers - 0x120 - 0x13F
// Hash .......................  64 Characters - 16 Registers - 0x140 - 0x17F
// Unused ..................... 144 Characters - 36 Registers - 0x180 - 0x20F
//                              ---              ---            -------------
//                              528              132          - 0x000 - 0x20F
//


`timescale 1 ns / 1 ps


module axi_identification #
 (
  // Parameters (AXI4-Lite)
  localparam c_addr_w           = 9,                       // P:Address Bus Width (bits)
  localparam c_data_w           = 32,                      // P:Data Bus Width (bits)
  // Parameters (ID Strings)
  localparam c_id_description_w = 128,                     // P:Description Width (chars)
  localparam c_id_company_w     = 64,                      // P:Company Width (chars)
  localparam c_id_author_w      = 64,                      // P:Author Width (chars)
  localparam c_id_version_w     = 32,                      // P:Version Width (chars)
  localparam c_id_timestamp_w   = 32,                      // P:Timestamp Width (chars)
  localparam c_id_hash_w        = 64                       // P:Hash Width (chars)
 )
 (
  // Clock & Reset
  input  wire                             aclk,            // I:Clock
  input  wire                             aresetn,         // I:Reset
  // Identification Strings
  input  wire [8*c_id_description_w-1 :0] id_description,  // I:Description
  input  wire [8*c_id_company_w-1 : 0]    id_company,      // I:Company
  input  wire [8*c_id_author_w-1 : 0]     id_author,       // I:Author
  input  wire [8*c_id_version_w-1 : 0]    id_version,      // I:Version
  input  wire [8*c_id_timestamp_w-1 : 0]  id_timestamp,    // I:Timestamp
  input  wire [8*c_id_hash_w-1 : 0]       id_hash,         // I:Hash
  // AXI4-Lite - Write Address Channel
  input  wire [c_addr_w-1 : 0]            s_axi_awaddr,    // I:Write Address
  input  wire [2 : 0]                     s_axi_awprot,    // I:Write Protection Type
  input  wire                             s_axi_awvalid,   // I:Write Address Valid
  output reg                              s_axi_awready,   // O:Write Address Ready
  // AXI4-Lite - Write Data Channel
  input  wire [c_data_w-1 : 0]            s_axi_wdata,     // I:Write Data
  input  wire [(c_data_w/8)-1 : 0]        s_axi_wstrb,     // I:Write Strobes
  input  wire                             s_axi_wvalid,    // I:Write Valid
  output reg                              s_axi_wready,    // O:Write Ready
  // AXI4-Lite - Write Response Channel
  output reg  [1 : 0]                     s_axi_bresp,     // O:Write Response
  output reg                              s_axi_bvalid,    // O:Write Response Valid
  input  wire                             s_axi_bready,    // I:Write Response Ready
  // AXI4-Lite - Read Address Channel
  input  wire [c_addr_w-1 : 0]            s_axi_araddr,    // I:Read Address
  input  wire [2 : 0]                     s_axi_arprot,    // I:Read Protection Type
  input  wire                             s_axi_arvalid,   // I:Read Address Valid
  output reg                              s_axi_arready,   // O:Read Address Ready
  // AXI4-Lite - Read Data Channel
  output reg  [c_data_w-1 : 0]            s_axi_rdata,     // O:Read Data
  output reg  [1 : 0]                     s_axi_rresp,     // O:Read Response
  output reg                              s_axi_rvalid,    // O:Read Valid
  input  wire                             s_axi_rready     // I:Read Ready
 );


  // Constants
  localparam no_regs = 2**(c_addr_w - $clog2(c_data_w / 8));
  localparam id_description_lo = 0;
  localparam id_description_hi = id_description_lo + (c_id_description_w / 4) - 1;
  localparam id_company_lo     = 1 + id_description_hi;
  localparam id_company_hi     = id_company_lo + (c_id_company_w / 4) - 1;
  localparam id_author_lo      = 1 + id_company_hi;
  localparam id_author_hi      = id_author_lo + (c_id_author_w / 4) -1;
  localparam id_version_lo     = 1 + id_author_hi;
  localparam id_version_hi     = id_version_lo + (c_id_version_w / 4) - 1;
  localparam id_timestamp_lo   = 1 + id_version_hi;
  localparam id_timestamp_hi   = id_timestamp_lo + (c_id_timestamp_w / 4) - 1;
  localparam id_hash_lo        = 1 + id_timestamp_hi;
  localparam id_hash_hi        = id_hash_lo + (c_id_hash_w / 4) - 1;


  // Signals
  reg aw_en;
  reg [c_addr_w-$clog2(c_data_w / 8) - 1 : 0] axi_awaddr;
  reg [c_addr_w-$clog2(c_data_w / 8) - 1 : 0] axi_araddr;
  reg [c_data_w-1 : 0] registers_wr [no_regs - 1 : 0];
  wire [c_data_w-1 : 0] registers_rd [no_regs - 1 : 0];


  // Write Address Ready
  always @(posedge aclk)
    if (!aresetn) begin
      s_axi_awready <= 1'b0;
      aw_en <= 1'b1;
    end else if (s_axi_awvalid && !s_axi_awready && s_axi_wvalid && aw_en) begin
      s_axi_awready <= 1'b1;
      aw_en <= 1'b0;
    end else if (s_axi_bvalid && s_axi_bready) begin
      s_axi_awready <= 1'b0;
      aw_en <= 1'b1;
    end else
      s_axi_awready <= 1'b0;


  // Write Address Latch
  always @(posedge aclk)
    if (!aresetn)
      axi_awaddr <= {c_addr_w{1'b0}};
    else if (s_axi_awvalid && !s_axi_awready && s_axi_wvalid && aw_en)
      axi_awaddr <= s_axi_awaddr[c_addr_w - 1 : $clog2(c_data_w/8)];


  // Write Data Ready
  always @(posedge aclk)
    if (!aresetn)
      s_axi_wready <= 1'b0;
    else if (s_axi_awvalid && s_axi_wvalid && !s_axi_wready && aw_en)
      s_axi_wready <= 1'b1;
    else
      s_axi_wready <= 1'b0;


  // Write Data
  integer i;
  always @(posedge aclk)
    if (!aresetn)
      for (i = 0; i <= no_regs - 1; i = i + 1)
        registers_wr[i] <= {c_data_w{1'b0}};
    else if (s_axi_awvalid && s_axi_awready && s_axi_wvalid && s_axi_wready)
      for (i = 0; i <= c_data_w/8 - 1; i = i + 1)
        if (s_axi_wstrb[i])
          registers_wr[axi_awaddr][i*8 +: 8] <= s_axi_wdata[i*8 +: 8]; // Byte-Wise Write


  // Write Response
  always @(posedge aclk)
    if (!aresetn) begin
      s_axi_bvalid <= 1'b0;
      s_axi_bresp  <= 2'b00;
    end else if (s_axi_awvalid && s_axi_awready && s_axi_wvalid && s_axi_wready && !s_axi_bvalid) begin
      s_axi_bvalid <= 1'b1;
      s_axi_bresp  <= 2'b00;
    end else if (s_axi_bvalid && s_axi_bready)
      s_axi_bvalid <= 1'b0;


  // Read Address Ready
  always @(posedge aclk)
    if (!aresetn)
      s_axi_arready <= 1'b0;
    else if (s_axi_arvalid && !s_axi_arready)
      s_axi_arready <= 1'b1;
    else
      s_axi_arready <= 1'b0;


  // Read Address Latch
  always @(posedge aclk)
    if (!aresetn)
      axi_araddr  <= {c_addr_w{1'b0}};
    else if (s_axi_arvalid && !s_axi_arready)
      axi_araddr <= s_axi_araddr[c_addr_w - 1 : $clog2(c_data_w/8)];


  // Read Response
  always @(posedge aclk)
    if (!aresetn) begin
      s_axi_rvalid <= 1'b0;
      s_axi_rresp  <= 2'b00;
    end else if (s_axi_arvalid && s_axi_arready && !s_axi_rvalid) begin
      s_axi_rvalid <= 1'b1;
      s_axi_rresp  <= 2'b00;
    end else if (s_axi_rvalid && s_axi_rready)
      s_axi_rvalid <= 1'b0;


  // Read Data
  always @(posedge aclk)
    if (!aresetn)
      s_axi_rdata <= {c_data_w{1'b0}};
    else if (s_axi_arvalid && s_axi_arready && !s_axi_rvalid)
      s_axi_rdata <= registers_rd[axi_araddr];


  // Read Register Constants
  genvar j;
  generate
    for (j = 0; j <= no_regs - 1; j = j + 1) begin
      if      (j <= id_description_hi) assign registers_rd[j] = id_description[32*(j-id_description_lo) +: 32];
      else if (j <= id_company_hi    ) assign registers_rd[j] = id_company    [32*(j-id_company_lo    ) +: 32];
      else if (j <= id_author_hi     ) assign registers_rd[j] = id_author     [32*(j-id_author_lo     ) +: 32];
      else if (j <= id_version_hi    ) assign registers_rd[j] = id_version    [32*(j-id_version_lo    ) +: 32];
      else if (j <= id_timestamp_hi  ) assign registers_rd[j] = id_timestamp  [32*(j-id_timestamp_lo  ) +: 32];
      else if (j <= id_hash_hi       ) assign registers_rd[j] = id_hash       [32*(j-id_hash_lo       ) +: 32];
      else                             assign registers_rd[j] = 32'h00000000;
    end
  endgenerate


endmodule 
