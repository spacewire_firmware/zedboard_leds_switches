//
// File .......... debounce.v
// Author ........ Steve Haywood
// Version ....... 1.0
// Date .......... 29 October 2022
// Standard ...... IEEE 1364-2001
// Description ...
//   Simple switch debouncer.
//


`timescale 1 ns / 1 ps


module debounce #
(
  // Parameters
  parameter  c_cycles = 65536  // Debounce Cycles
)
(
  // Clock & Reset
  input      aclk,             // Clock
  input      aresetn,          // Reset
  // Input & Output
  input      inp,              // Input (Dirty)
  output reg outp              // Output (Clean)
);


  // Signals
  reg inp_meta;
  reg inp_int;
  reg [$clog2(c_cycles)-1:0] count;


  // Metastability hardener
  always @(posedge aclk)
    if (!aresetn) begin
      inp_meta <= 1'b0;
      inp_int  <= 1'b0;
    end else begin
      inp_meta <= inp;
      inp_int  <= inp_meta;
     end


  // Debouncer
  always @(posedge aclk)
    if (!aresetn) begin
      outp  <= 1'b0;
      count <= 32'b0;
    end else if (outp == inp_int) begin
      count <= 32'b0;
    end else if (count == c_cycles - 1) begin
      outp  <= inp;
      count <= 32'b0;
    end else
      count <= count + 1'b1;


endmodule
