#
# File .......... pre_synth.tcl
# Author ........ Steve Haywood
# Version ....... 1.1
# Date .......... 2 October 2022
# Description ...
#   Script to read product/project information from a text file, obtain GIT
# repository information and set the generics/parameters on the top level
# module.
#

# Read Product/Project Information
if { [catch {set id_file [open "../../../project.txt" r]} msg] } {
  set id_description "?"
  set id_company "?"
  set id_author "?"
  set id_version "?"
  puts "ERROR :-"
  puts $msg
} {
  gets $id_file id_description
  gets $id_file id_company
  gets $id_file id_author
  gets $id_file id_version
  close $id_file
}

# Get GIT timestamp
set dfmt "%d-%b-%Y - %H:%M:%S"
if { [catch {set id_timestamp [exec git log -1 --pretty=%cd --date=format:$dfmt]} msg] } {
  set id_timestamp "00-Xxx-0000 - 00:00:00"
  puts "ERROR :-"
  puts $msg
}

# Get GIT hash
if { [catch {set id_hash [exec git log -1 --pretty=%H]} msg] } {
  set id_hash "0000000000000000000000000000000000000000"
  puts "ERROR :-"
  puts $msg
}

# Get GIT status
if { [catch {set status [exec git status -s]} msg] } {
  append id_version " (undefined)"
  puts "ERROR :-"
  puts $msg
} {
  if {$status ne ""} {
    append id_version " (unstaged)"
  } {
    if { [catch {set status [exec git branch -r --contains $id_hash]} msg] } {
      append id_version " (undefined)"
      puts "ERROR :-"
      puts $msg
    } {
      if {$status eq ""} {
        append id_version " (unpushed)"
      }
    }
  }
}

# Replace space character with its octal code
proc space_replace {str} {
  return [string map {" " "\\040"} $str]
}

# Replace spaces in information strings
set id_description [space_replace $id_description]
set id_company     [space_replace $id_company]
set id_author      [space_replace $id_author]
set id_version     [space_replace $id_version]
set id_timestamp   [space_replace $id_timestamp]
set id_hash        [space_replace $id_hash]

# Set Generics/Parameters
set_property generic " \
  id_description=\"$id_description\" \
  id_company=\"$id_company\" \
  id_author=\"$id_author\" \
  id_version=\"$id_version\" \
  id_timestamp=\"$id_timestamp\" \
  id_hash=\"$id_hash\" \
" [current_fileset]
