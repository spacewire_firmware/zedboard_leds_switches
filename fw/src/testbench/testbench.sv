//
// File .......... testbench.sv
// Author ........ Steve Haywood
// Version ....... 3.0
// Date .......... 22 December 2021
// Description ...
//   Very simple testbench to check correct connectivity of the block diagram.
// Uses the Zynq Verification IP suite to read/write the GPIO LED register and
// read the GPIO switch register.
// 


timeunit      1ns;
timeprecision 1ps;


module testbench;

  // Constants
  localparam [31:0] led_addr = 32'h41200000;
  localparam [31:0] sws_addr = 32'h41200008;
  localparam [31:0] led_init = 32'h00000018;
  localparam [31:0] led_next = 32'h00000081;
  localparam [ 7:0] sws_next =  8'b11000011;
  localparam [31:0] reg_addr = 32'h43C00000;


  // Signals
  wire       ps_clk;
  wire       ps_porb;
  wire       ps_srstb;
  bit [ 7:0] leds_8bits;
  bit [ 7:0] sws_8bits;
  bit        clk;
  bit        resetn;
  bit        bresp;
  bit [31:0] rdata;
  bit        rresp;


  // 50MHz Clock
  always #10 clk = !clk;


  // Reset
  initial begin
    repeat(20)
      @(posedge clk);
    resetn = 1'b1;
  end


  // Drive PL Clock & Reset
  assign ps_clk   = clk;
  assign ps_porb  = resetn;
  assign ps_srstb = resetn;


  // Stimulus
  initial begin
    // Allow reset time to work

    @(posedge resetn);
    repeat(10)
      @(posedge clk);

    // Reset PL
    testbench.system_wrapper_i.system_i.processing_system7_0.inst.fpga_soft_reset(32'h1);
    testbench.system_wrapper_i.system_i.processing_system7_0.inst.fpga_soft_reset(32'h0);

    // Read GPIO LED Register
    testbench.system_wrapper_i.system_i.processing_system7_0.inst.read_data(led_addr, 4, rdata, rresp);
    $display ("Read of GPIO LED Register = 32'h%x (%s)", rdata, (rdata == led_init) ? "expected" : "unexpected");

    // Write GPIO LED Register
    testbench.system_wrapper_i.system_i.processing_system7_0.inst.write_data(led_addr, 4, led_next, bresp);

    // Read GPIO LED Register
    testbench.system_wrapper_i.system_i.processing_system7_0.inst.read_data(led_addr, 4, rdata, rresp);
    $display ("Read of GPIO LED Register = 32'h%x (%s)", rdata, (rdata == led_next) ? "expected" : "unexpected");

    // Set some of the switches to 'on'
    sws_8bits = sws_next;

    // Read GPIO Switch Register
    testbench.system_wrapper_i.system_i.processing_system7_0.inst.read_data(sws_addr, 4, rdata, rresp);
    $display ("Read of GPIO Switch Register = 32'h%x (%s)", rdata, (rdata == sws_next) ? "expected" : "unexpected");

    // Write Register Bank Registers
    for (int i = 0; i < 'h10; i = i + 4)
      testbench.system_wrapper_i.system_i.processing_system7_0.inst.write_data(reg_addr + i, 4, 65536 + i, bresp);

    // Read Register Bank Register
    for (int i = 0; i < 'h10; i = i + 4) begin
      testbench.system_wrapper_i.system_i.processing_system7_0.inst.read_data(reg_addr, 4, rdata, rresp);
      $display ("Read of GPIO LED Register = 32'h%x (%s)", rdata, (rdata == 65536 + i) ? "expected" : "unexpected");
    end

    // All done
    $display ("That's all folks!");
    $stop;

  end


  // Unit Under Test
  system_wrapper #
   (
    // Parameters
    .id_description ( "Description" ),  // P:Firmware Description ... Max 128 Characters
    .id_company     ( "Company"     ),  // P:Company ................ Max  64 Characters
    .id_author      ( "Author"      ),  // P:Author ................. Max  64 Characters
    .id_version     ( "Version"     ),  // P:Version Number ......... Max  16 Characters
    .id_timestamp   ( "Timestamp"   )   // P:Build Timestamp ........ Max  32 Characters
   )
  system_wrapper_i
   (
    // LEDs
    .leds_tri_o        ( leds_8bits ),  // O:LEDs
    // Switches
    .switches_tri_i    ( sws_8bits  ),  // I:Switches
    // System
    .DDR_addr          (            ),  // B:Address
    .DDR_ba            (            ),  // B:Bank Address
    .DDR_cas_n         (            ),  // B:Column Address Select
    .DDR_ck_n          (            ),  // B:Clock (Neg)
    .DDR_ck_p          (            ),  // B:Clock (Pos)
    .DDR_cke           (            ),  // B:Clock Enable
    .DDR_cs_n          (            ),  // B:Chip Select
    .DDR_dm            (            ),  // B:Data Mask
    .DDR_dq            (            ),  // B:Data Input/Output
    .DDR_dqs_n         (            ),  // B:Data Strobe (Neg)
    .DDR_dqs_p         (            ),  // B:Data Strobe (Pos)
    .DDR_odt           (            ),  // B:Output Dynamic Termination
    .DDR_ras_n         (            ),  // B:Row Address Select
    .DDR_reset_n       (            ),  // B:Reset
    .DDR_we_n          (            ),  // B:Write Enable
    .FIXED_IO_ddr_vrn  (            ),  // B:Termination Voltage
    .FIXED_IO_ddr_vrp  (            ),  // B:Termination Voltage
    .FIXED_IO_mio      (            ),  // B:Peripheral Input/Output
    .FIXED_IO_ps_clk   ( ps_clk     ),  // B:System Reference Clock
    .FIXED_IO_ps_porb  ( ps_porb    ),  // B:Power On Reset
    .FIXED_IO_ps_srstb ( ps_srstb   )   // B:External System Reset
   );


endmodule
