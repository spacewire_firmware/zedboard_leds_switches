# User LEDs - Bank 33
set_property PACKAGE_PIN T22 [get_ports {leds[0]}];  # "LD0"
set_property PACKAGE_PIN T21 [get_ports {leds[1]}];  # "LD1"
set_property PACKAGE_PIN U22 [get_ports {leds[2]}];  # "LD2"
set_property PACKAGE_PIN U21 [get_ports {leds[3]}];  # "LD3"
set_property PACKAGE_PIN V22 [get_ports {leds[4]}];  # "LD4"
set_property PACKAGE_PIN W22 [get_ports {leds[5]}];  # "LD5"
set_property PACKAGE_PIN U19 [get_ports {leds[6]}];  # "LD6"
set_property PACKAGE_PIN U14 [get_ports {leds[7]}];  # "LD7"


# User Push Buttons - Bank 34
set_property PACKAGE_PIN P16 [get_ports {buttons[0]}];  # "BTNC"
set_property PACKAGE_PIN R16 [get_ports {buttons[1]}];  # "BTND"
set_property PACKAGE_PIN N15 [get_ports {buttons[2]}];  # "BTNL"
set_property PACKAGE_PIN R18 [get_ports {buttons[3]}];  # "BTNR"
set_property PACKAGE_PIN T18 [get_ports {buttons[4]}];  # "BTNU"


# User DIP Switches - Bank 34 & 35
set_property PACKAGE_PIN F22 [get_ports {switches[0]}];  # "SW0"
set_property PACKAGE_PIN G22 [get_ports {switches[1]}];  # "SW1"
set_property PACKAGE_PIN H22 [get_ports {switches[2]}];  # "SW2"
set_property PACKAGE_PIN F21 [get_ports {switches[3]}];  # "SW3"
set_property PACKAGE_PIN H19 [get_ports {switches[4]}];  # "SW4"
set_property PACKAGE_PIN H18 [get_ports {switches[5]}];  # "SW5"
set_property PACKAGE_PIN H17 [get_ports {switches[6]}];  # "SW6"
set_property PACKAGE_PIN M15 [get_ports {switches[7]}];  # "SW7"


# Banks
set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 33]];
set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 34]];
set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 35]];
