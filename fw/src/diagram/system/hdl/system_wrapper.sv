//
// File .......... system_wrapper.sv
// Author ........ Steve Haywood
// Version ....... 1.2
// Date .......... 29 October 2022
// Standard ...... IEEE 1800-2017
// Description ...
//   Top level wrapper for the underlying block design.
//


timeunit      1ns;
timeprecision 1ps;


module system_wrapper #
(
  // Parameters
  parameter string id_description = "",  // P:Description ............ Max 128 Characters
  parameter string id_company     = "",  // P:Company ................ Max  64 Characters
  parameter string id_author      = "",  // P:Author ................. Max  64 Characters
  parameter string id_version     = "",  // P:Version ................ Max  32 Characters
  parameter string id_timestamp   = "",  // P:Timestamp .............. Max  32 Characters
  parameter string id_hash        = ""   // P:Hash ................... Max  64 Characters
)
(
  // LEDs
  output [ 7:0] leds,               // O:LEDs
  // DIP Switches
  input  [ 7:0] switches,           // I:DIP Switches
  // Push Buttons
  input  [ 4:0] buttons,            // I:Push Buttons
  // System
  inout  [14:0] DDR_addr,           // B:Address
  inout  [ 2:0] DDR_ba,             // B:Bank Address
  inout         DDR_cas_n,          // B:Column Address Select
  inout         DDR_ck_n,           // B:Clock (Neg)
  inout         DDR_ck_p,           // B:Clock (Pos)
  inout         DDR_cke,            // B:Clock Enable
  inout         DDR_cs_n,           // B:Chip Select
  inout  [ 3:0] DDR_dm,             // B:Data Mask
  inout  [31:0] DDR_dq,             // B:Data Input/Output
  inout  [ 3:0] DDR_dqs_n,          // B:Data Strobe (Neg)
  inout  [ 3:0] DDR_dqs_p,          // B:Data Strobe (Pos)
  inout         DDR_odt,            // B:Output Dynamic Termination
  inout         DDR_ras_n,          // B:Row Address Select
  inout         DDR_reset_n,        // B:Reset
  inout         DDR_we_n,           // B:Write Enable
  inout         FIXED_IO_ddr_vrn,   // B:Termination Voltage
  inout         FIXED_IO_ddr_vrp,   // B:Termination Voltage
  inout  [53:0] FIXED_IO_mio,       // B:Peripheral Input/Output
  inout         FIXED_IO_ps_clk,    // B:System Reference Clock
  inout         FIXED_IO_ps_porb,   // B:Power On Reset
  inout         FIXED_IO_ps_srstb   // B:External System Reset
);


  // Function to convert a string to a vector (max 128 characters)
  function [1023:0] fmt(input string str);
    int len;
    bit [1023:0] tmp;
    len = str.len();
    for (int i=0; i<len; i++)
      tmp[8*i +: 8] = str.getc(i);
    fmt = tmp;
  endfunction


  // Top-Level Block Design
  system system_i
   (
    // Identification Strings
    .id_description    ( fmt(id_description) ),  // P:Description
    .id_company        ( fmt(id_company)     ),  // P:Company
    .id_author         ( fmt(id_author)      ),  // P:Author
    .id_version        ( fmt(id_version)     ),  // P:Version
    .id_timestamp      ( fmt(id_timestamp)   ),  // P:Timestamp
    .id_hash           ( fmt(id_hash)        ),  // P:Hash
    // LEDs
    .leds              ( leds                ),  // O:LEDs
    // DIP Switches
    .switches          ( switches            ),  // I:DIP Switches
    // Push Buttons
    .buttons           ( buttons             ),  // I:Push Buttons
    // System
    .DDR_addr          ( DDR_addr            ),  // B:Address
    .DDR_ba            ( DDR_ba              ),  // B:Bank Address
    .DDR_cas_n         ( DDR_cas_n           ),  // B:Column Address Select
    .DDR_ck_n          ( DDR_ck_n            ),  // B:Clock (Neg)
    .DDR_ck_p          ( DDR_ck_p            ),  // B:Clock (Pos)
    .DDR_cke           ( DDR_cke             ),  // B:Clock Enable
    .DDR_cs_n          ( DDR_cs_n            ),  // B:Chip Select
    .DDR_dm            ( DDR_dm              ),  // B:Data Mask
    .DDR_dq            ( DDR_dq              ),  // B:Data Input/Output
    .DDR_dqs_n         ( DDR_dqs_n           ),  // B:Data Strobe (Neg)
    .DDR_dqs_p         ( DDR_dqs_p           ),  // B:Data Strobe (Pos)
    .DDR_odt           ( DDR_odt             ),  // B:Output Dynamic Termination
    .DDR_ras_n         ( DDR_ras_n           ),  // B:Row Address Select
    .DDR_reset_n       ( DDR_reset_n         ),  // B:Reset
    .DDR_we_n          ( DDR_we_n            ),  // B:Write Enable
    .FIXED_IO_ddr_vrn  ( FIXED_IO_ddr_vrn    ),  // B:Termination Voltage
    .FIXED_IO_ddr_vrp  ( FIXED_IO_ddr_vrp    ),  // B:Termination Voltage
    .FIXED_IO_mio      ( FIXED_IO_mio        ),  // B:Peripheral Input/Output
    .FIXED_IO_ps_clk   ( FIXED_IO_ps_clk     ),  // B:System Reference Clock
    .FIXED_IO_ps_porb  ( FIXED_IO_ps_porb    ),  // B:Power On Reset
    .FIXED_IO_ps_srstb ( FIXED_IO_ps_srstb   )   // B:External System Reset
   );


endmodule
